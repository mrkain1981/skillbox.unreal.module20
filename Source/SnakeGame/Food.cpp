// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include <ctime>

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			if (FoodType == EFoodType::SIMPLE)
			{
				Snake->AddSnakeElement();
			}
			else
			{
				Snake->AddComplexElement();
			}

			
			//srand(time(0));
			//FVector NewLocation(-400 + std::rand() % 400, -400 + std::rand() % 400, 0);
			//FTransform NewTransform(NewLocation);
			//AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
			//NewFood->FoodType = EFoodType::SIMPLE;

			this->Destroy();
		}
		
	}
}

