// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeElementBase.generated.h"

class UStaticMeshComponent;
class ASnakeBase;


UENUM(BlueprintType)
enum class EElementType : uint8
{
	LEVEL_1     UMETA(DisplayName = "Level 1"), 
	LEVEL_2     UMETA(DisplayName = "Level 2"),
	LEVEL_3     UMETA(DisplayName = "Level 3")
};

UCLASS()
class SNAKEGAME_API ASnakeElementBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElementBase();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;

	UPROPERTY()
		ASnakeBase* SnakeOwner;

	int32 ElementIndex;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
	EElementType ElementType;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent)
		void SetFirstElementType();
	    void SetFirstElementType_Implementation();

		UFUNCTION(BlueprintNativeEvent)
			void SetElementType(EElementType NewElementType);
			void SetElementType_Implementation(EElementType NewElementType);

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	UFUNCTION()
		void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, 
			                    AActor* OtherActor, 
			                    UPrimitiveComponent* OtherComp,
			                    int32 OtherBodyIndex,
								bool BFromSweep,
								const FHitResult &SweepResult
		);

	UFUNCTION()
		void ToggleCollision();
};
