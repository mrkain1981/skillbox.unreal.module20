// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Food.generated.h"


UENUM(BlueprintType)
enum class EFoodType: uint8
{
	SIMPLE     UMETA(DisplayName = "Simple"), // Increasing number of snake elements (Level 1)
	COMPLEX    UMETA(DisplayName = "Complex") // Improving quality of snake element (Level 2)
};

UCLASS()
class SNAKEGAME_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EFoodType FoodType;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodClass;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

};
