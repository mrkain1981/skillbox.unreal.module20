// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"

// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElementBase::SetFirstElementType_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}

void ASnakeElementBase::SetElementType_Implementation(EElementType NewElementType)
{
	ElementType = NewElementType;
}

void ASnakeElementBase::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			switch (ElementType)
			{
			case EElementType::LEVEL_1:
				for (int i = Snake->SnakeElements.Num() - 1; i >= ElementIndex; i--)
				{
					Snake->SnakeElements[i]->Destroy();
					Snake->SnakeElements.RemoveAt(i);
				}
				break;
			case EElementType::LEVEL_2:
				Snake->AddSuperElement();
				break;
			case EElementType::LEVEL_3:
				Snake->Destroy();
				break;
			default:
				break;
			}
		}
	}
}

void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, 
										   AActor* OtherActor, 
	                                       UPrimitiveComponent* OtherComp, 
	                                       int32 OtherBodyIndex, 
	                                       bool BFromSweep, 
	                                       const FHitResult& SweepResult)
{
	if (IsValid(SnakeOwner)) 
	{
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
	}
}

void ASnakeElementBase::ToggleCollision()
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
	
}

